package com.bls.payment.controller;

import com.bls.payment.config.WxPayConfig;
import com.bls.payment.vo.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;
import java.security.PrivateKey;

@RestController
@RequestMapping("/api/test")
@Slf4j
public class TestController {

    @Resource
    private WxPayConfig wxPayConfig;

    @GetMapping
    public R getWxPayConfig(){
        String mchId = wxPayConfig.getMchId();
        log.info("商户Id是：" + mchId);
        return R.ok().data("mchId", mchId);
    }

    @GetMapping("/getPrivateKey")
    public R getPrivateKey(){
        String privateKeyPath = wxPayConfig.getPrivateKeyPath();
        PrivateKey privateKey = wxPayConfig.getPrivateKey(privateKeyPath);
        log.info("密钥文件对象：" + privateKey);
        return R.ok().data("privateKey", privateKey);
    }
}
