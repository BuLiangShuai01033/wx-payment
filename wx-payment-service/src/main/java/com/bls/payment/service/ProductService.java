package com.bls.payment.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bls.payment.entity.Product;

public interface ProductService  extends IService<Product> {
}
