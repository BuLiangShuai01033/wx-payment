package com.bls.payment.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bls.payment.entity.RefundInfo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RefundInfoMapper extends BaseMapper<RefundInfo> {

}
