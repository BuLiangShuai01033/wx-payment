package com.bls.payment.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bls.payment.entity.PaymentInfo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PaymentInfoMapper extends BaseMapper<PaymentInfo> {
}
